﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public bool isGameOver;

    int score;

    Rigidbody rb;

    public static int remainingCollectables;

    float restartTimer;
    float duration;
    public float restartDelay = 5f;

    AudioSource audioSource;

    public AudioClip audioClipMetal;
    public AudioClip audioClipWood;
    public AudioClip audioClipBeep;
    public AudioClip audioClipVictory;
    public AudioClip audioClipLoss;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        audioSource = GetComponent<AudioSource>();

        isGameOver = false;

        remainingCollectables = GameObject.FindGameObjectsWithTag("Collectable").Length * 2;
        Debug.Log("Remaining collectables: " + remainingCollectables);
    }

    void FixedUpdate()
    {
        if (remainingCollectables <= 0 && !isGameOver)
            GameWon();
        
        // Input controls
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // X,Y,Z; Y is zero because we do not want to move up or down by Y axis.
        // Produce movement vecor
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        // Push
        rb.AddForce(movement * speed);
    }

    void OnCollisionEnter(Collision collision)
    {

        Debug.Log("Collided with " + collision.gameObject.tag);

        if (collision.gameObject.tag == "Border")
        {
            audioSource.PlayOneShot(audioClipMetal);
        }

        if (collision.gameObject.tag == "Board")
        {
            audioSource.PlayOneShot(audioClipWood);
        }

        if (collision.gameObject.tag == "Collectable")
        {
            audioSource.PlayOneShot(audioClipBeep);
        }

        if (collision.gameObject.tag == "Outside" && !isGameOver)
        {
            GameOver();
        }
    }

    void GameOver()
    {
        Debug.Log("Game Over");
        isGameOver = true;
        audioSource.PlayOneShot(audioClipLoss);
        duration = audioClipLoss.length;
        StartCoroutine(WaitForSound());
    }

    void GameWon()
    {
        Debug.Log("Game Win");
        isGameOver = true;
        audioSource.PlayOneShot(audioClipVictory);
        duration = audioClipVictory.length;
        StartCoroutine(WaitForSound());
    }


    IEnumerator WaitForSound()
    {
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene("Game");
    }

}