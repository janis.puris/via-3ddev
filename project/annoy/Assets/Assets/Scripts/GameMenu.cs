﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{

    public static bool GameIsPaused = true;

    public GameObject pauseMenu;
    public GameObject bgAudio;

    public Text resumeButtonText;

    AudioSource m_AudioSource;

	// Update is called once per frame

	void Start()
	{
        m_AudioSource = bgAudio.GetComponent<AudioSource>();
        Pause();

	}

	void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

        m_AudioSource.Play();

        resumeButtonText.text= "Resume";
    }

    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

        m_AudioSource.Pause();

        if(GameObject.Find("Player").GetComponent("isPlayerDead") == true)
        {
            //GameObject.Find("GameOverText").SetActive(true);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
