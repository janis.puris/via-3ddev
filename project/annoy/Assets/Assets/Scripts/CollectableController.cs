﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour {

    public AudioClip audioClipDead;
    AudioSource audioSource;

    bool isDead;

    void Start()
    {
        isDead = false;
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Outside" && !isDead)
        {
            isDead = true;
            audioSource.PlayOneShot(audioClipDead);
            PlayerController.remainingCollectables -= 1;
            Debug.Log(PlayerController.remainingCollectables);
        }
    }
}
