﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour {

    public GameObject theObject;
    public Vector3 spawnValues;
    public bool stop;

    bool isDead;

	void Start()
	{
        isDead = false;
	}

	void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && !isDead)
        {
            Debug.Log("Collectable has collided with object. Tag: " + collision.gameObject.tag);

            isDead = true;

            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 5.0f, Random.Range(-spawnValues.z, spawnValues.z));
            Debug.Log("Collectable spawned (" + Random.Range(-spawnValues.x, spawnValues.x) + "," + spawnValues.y + "," + Random.Range(-spawnValues.z, spawnValues.z) + ")");

            Instantiate(theObject, spawnPosition, gameObject.transform.rotation);
        }
    }

}
