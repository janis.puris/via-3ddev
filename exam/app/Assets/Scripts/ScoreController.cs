﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    int scoreBest;
    int score;

    public Text scoreBestText;
    public Text scoreText;

	void Start () {
        
        score = PlayerPrefs.GetInt("Last", 0);
        scoreBest = PlayerPrefs.GetInt("Best", score);
        scoreBestText.text = "Best: " + scoreBest;
        scoreText.text = "Last: " + score;
		
	}

	void Update () {

        if (score > scoreBest)
        {
            scoreBest = score;
            scoreBestText.text = "Best: " + scoreBest;
        }

	}

    public void AddScore(int increment)
    {
        score += increment;
        scoreText.text = "Last: " + score;
    }

    void OnDestroy()
    {
        PlayerPrefs.SetInt("Best", scoreBest);
        PlayerPrefs.SetInt("Last", score);
        PlayerPrefs.Save();
    }
}
