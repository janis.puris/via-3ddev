﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyController : MonoBehaviour
{

     public Transform Player;
     int MoveSpeed = 4;

    void Update()
    {
        transform.LookAt(Player);

        if (Vector3.Distance(transform.position, Player.position) >= 0)
            transform.position += transform.forward * MoveSpeed * Time.deltaTime;
    }
}