﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public bool isGameOver;

    public int score;

    Rigidbody rb;

    ScoreController scoreScript;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        isGameOver = false;
        score = 0;

        scoreScript = GetComponent<ScoreController>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" && !isGameOver)
        {
            isGameOver = true;
            GameOver();
        }

        if (collision.gameObject.tag == "Collectable")
        {
            Destroy(collision.gameObject);

            scoreScript.AddScore(1);

            Debug.Log("Score : " + score);
        }
            
    }

    void GameOver()
    {
        Debug.Log("Game Over");
        isGameOver = true;
        SceneManager.LoadScene("scene_1");
    }

    void GameWon()
    {
        Debug.Log("Game Win");
        isGameOver = true;
    }

}