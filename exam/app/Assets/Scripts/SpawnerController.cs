﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    public GameObject[] theObject;
    public Vector3 spawnValues;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

	void Start()
    {
        StartCoroutine(waitSpawner());
	}
	

	void Update()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
	}

    IEnumerator waitSpawner()
    {
        yield return new WaitForSeconds(startWait);

        while (!stop)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 5.0f, Random.Range(-spawnValues.z, spawnValues.z));
            Debug.Log("Enemy spawned (" + Random.Range(-spawnValues.x, spawnValues.x) + "," + spawnValues.y + "," + Random.Range(-spawnValues.z, spawnValues.z) + ")");

            SpawnObject(theObject[0],spawnPosition, gameObject.transform.rotation);

            yield return new WaitForSeconds(spawnWait);
        }
    }

    void SpawnObject(GameObject localObject,Vector3 localSpawnPosition, Quaternion localRotation)
    {
        Instantiate(localObject, localSpawnPosition, localRotation);
    }
}
